import { type Metadata } from 'next';
import './globals.css';
import { Container } from '@radix-ui/themes';

import Providers from '@/providers/app';
import Navigation from '@/components/navigation-menu';

export const metadata: Metadata = {
	metadataBase: new URL(`${process.env.DOMAIN_URL}`),
	title: {
		template: '%s | Dormswap',
		default: 'Dormswap'
	},
	openGraph: {
		title: 'Dormswap',
		description: 'Dormswap - Trade your stuff with other Brno students',
		siteName: 'Dormswap',

		type: 'website'
	}
};

const RootLayout = ({ children }: { children: React.ReactNode }) => (
	<html lang="en" suppressHydrationWarning>
		<body>
			<Providers>
				<div>
					<Navigation />
					<main className="flex min-h-screen flex-col items-center gap-5 p-24">
						<Container className="pt-12">{children} </Container>
					</main>
				</div>
			</Providers>
		</body>
	</html>
);

export default RootLayout;
