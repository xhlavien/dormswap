import React from 'react';
import LoadingSpinner from '@/components/loading-spinner';

const loading = () => <LoadingSpinner label="Loading..." />;
export default loading;
