import { redirect } from 'next/navigation';
import { type PropsWithChildren } from 'react';

import { getServerAuthSession } from '@/server/auth';

const PrivateLayout = async ({ children }: PropsWithChildren) => {
	const status = await getServerAuthSession();
	if (!status) {
		// User unauthenticated, redirect to login
		redirect('/login');
	}

	return <div>{children}</div>;
};

export default PrivateLayout;
