import React from 'react';
import { redirect } from 'next/navigation';

import DashboardContainer from '@/components/dashboard-container';
import LoadingSpinner from '@/components/loading-spinner';
import { getServerAuthSession } from '@/server/auth';
import { db } from '@/server/db';

const Dashboard = async ({
	searchParams
}: {
	searchParams: {
		[key: string]: string | string[] | undefined;
	};
}) => {
	const status = await getServerAuthSession();
	if (!status) {
		// User unauthenticated, redirect to login
		redirect('/login');
	}

	const user = await db.user.findUnique({ where: { id: status.user.id } });

	if (!user) {
		redirect('/login');
	}
	const query =
		typeof searchParams.query === 'string' ? searchParams.query : undefined;

	return <DashboardContainer query={query} userId={user.id} />;
};

export default Dashboard;
