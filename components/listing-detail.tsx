'use client';
import React, { type PropsWithChildren } from 'react';
import { Avatar, Box, Card, Flex, Heading, Text } from '@radix-ui/themes';
import Image from 'next/image';
import { type Listing } from '.prisma/client';

import { useFindUserByIdQuery } from '@/hooks/useFindUserByIdQuery';
import LoadingSpinner from '@/components/loading-spinner';
import { useGetProductByIdQuery } from '@/hooks/useGetProductByIdQuery';

type ListingDetailProps = {
	listing: Listing;
} & PropsWithChildren;

const ListingDetail = ({ listing, children }: ListingDetailProps) => {
	const {
		data: user,
		isError: isErrorUser,
		isLoading: isLoadingUser
	} = useFindUserByIdQuery(listing.userId);

	const {
		data: product,
		isLoading: isLoadingProduct,
		isError: isErrorProduct
	} = useGetProductByIdQuery(listing.offeredProductId);

	if (isLoadingUser || isLoadingProduct) {
		return <LoadingSpinner label="Loading listing and product..." />;
	}

	if (!user || isErrorUser || !product || isErrorProduct) {
		return <h2 className="text-white">Error loading data.</h2>;
	}

	return (
		<Box key={product.productId} className="mx-auto my-8 w-full ">
			<Card className="max-w-[600px] rounded-md bg-white p-6 shadow-md">
				<Flex direction="column" gap="4">
					{user && (
						<Flex align="center" gap="2">
							<Avatar
								src={user.image ?? undefined}
								alt={user.name ?? undefined}
								size="3"
								fallback=""
							/>
							<Text>{user.name}</Text>
						</Flex>
					)}

					<Heading as="h1" size="4" className="mb-2 text-3xl font-bold">
						{product.name}
					</Heading>

					<Text size="3" className="mb-4 text-gray-500">
						{product.description}
					</Text>

					<Image
						className="h-auto max-h-[75%] w-full rounded-md object-cover shadow-md"
						src={product.photo}
						alt={product.name}
						width={1000}
						height={1000}
					/>

					{children}
				</Flex>
			</Card>
		</Box>
	);
};

export default ListingDetail;
