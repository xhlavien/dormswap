'use client';
import * as NavigationMenu from '@radix-ui/react-navigation-menu';
import { Switch, Box, Text } from '@radix-ui/themes';
import { SunIcon, MoonIcon } from '@radix-ui/react-icons';
import Link from 'next/link';
import { useTheme } from 'next-themes';
import { signIn, signOut, useSession } from 'next-auth/react';
import { redirect } from 'next/navigation';
import React from 'react';

const Navigation = () => {
	const { setTheme, resolvedTheme } = useTheme();
	const { data, status } = useSession();

	const handleSignIn = () => {
		signIn().then(() => redirect('/'));
	};

	const handleSignOut = () => {
		signOut()
			.then(() => redirect('/'))
			.catch(err => console.log(err));
	};

	return (
		<NavigationMenu.Root
			className="fixed z-50
    flex w-full items-center justify-between
    bg-white bg-opacity-50 py-2 text-gray-900 backdrop-blur-lg backdrop-filter dark:bg-teal-950 dark:bg-opacity-50 dark:text-gray-100"
		>
			<Box className="invisible mr-5 flex items-center">
				<Box>
					{status === 'loading' ? (
						<p>Loading...</p>
					) : data?.user ? (
						<button onClick={handleSignOut}>Sign out</button>
					) : (
						<button onClick={handleSignIn}>Sign In</button>
					)}
				</Box>
				{resolvedTheme && (
					<Box className="flex items-center gap-2">
						<Switch
							className="cursor-pointer"
							checked={resolvedTheme === 'dark'}
							name="dark-mode"
							onCheckedChange={e => setTheme(e ? 'dark' : 'light')}
							id="dark-mode"
						/>
						<Box>{resolvedTheme === 'dark' ? <MoonIcon /> : <SunIcon />}</Box>
					</Box>
				)}
			</Box>
			<NavigationMenu.List className="flex  gap-5">
				<NavigationMenu.Item className="  font-normal text-gray-400 transition-all hover:text-gray-900 dark:text-gray-300 dark:hover:text-gray-100 ">
					<Link href="/"><Text className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Home</Text></Link>
				</NavigationMenu.Item>
				<NavigationMenu.Item className="  font-normal text-gray-400 transition-all hover:text-gray-900 dark:text-gray-300 dark:hover:text-gray-100 ">
					<Link href="/dashboard"><Text className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Dashboard</Text></Link>
				</NavigationMenu.Item>
				<NavigationMenu.Item className="  font-normal text-gray-400 transition-all hover:text-gray-900 dark:text-gray-300 dark:hover:text-gray-100 ">
					<Link href="/listings"><Text className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Listings</Text></Link>
				</NavigationMenu.Item>
				<NavigationMenu.Item className="  font-normal text-gray-400 transition-all hover:text-gray-900 dark:text-gray-300 dark:hover:text-gray-100 ">
					<Link href="/profile"><Text className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Profile</Text></Link>
				</NavigationMenu.Item>
				<NavigationMenu.Item className="  font-normal text-gray-400 transition-all hover:text-gray-900 dark:text-gray-300 dark:hover:text-gray-100 ">
					<Link href="/inventory"><Text className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Inventory</Text></Link>
				</NavigationMenu.Item>
				<NavigationMenu.Indicator className="NavigationMenuIndicator" />
			</NavigationMenu.List>
			<Box className="mr-5 flex items-center gap-8">
				<Box>
					{status === 'loading' ? (
						<p>Loading...</p>
					) : data?.user ? (
						<button onClick={handleSignOut} className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Sign out</button>
					) : (
						<button onClick={handleSignIn} className={resolvedTheme === 'dark' ? "text-white" : "text-black"}>Sign In</button>
					)}
				</Box>
				{resolvedTheme && (
					<Box className="flex items-center gap-2">
						<Switch
							checked={resolvedTheme === 'dark'}
							name="dark-mode"
							onCheckedChange={e => setTheme(e ? 'dark' : 'light')}
							id="dark-mode"
						/>
						<Box>{resolvedTheme === 'dark' ? <MoonIcon /> : <SunIcon />}</Box>
					</Box>
				)}
			</Box>
			<NavigationMenu.Viewport />
		</NavigationMenu.Root>
	);
};
export default Navigation;
