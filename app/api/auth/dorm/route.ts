import { NextResponse } from 'next/server';

import { db } from '@/server/db';

export const GET = async (request: Request) => {
	const dorms = await db.dorm.findMany();

	if (!dorms) {
		return NextResponse.error();
	}

	return NextResponse.json({ status: 200, body: dorms });
};
