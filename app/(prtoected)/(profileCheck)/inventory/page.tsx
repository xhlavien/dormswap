import React from 'react';

import Inventory from '@/components/inventory/inventory';
const InventoryPage = () => <Inventory />;

export default InventoryPage;
