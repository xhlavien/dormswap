import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { type Listing } from '.prisma/client';

export const useGetListings = () =>
	useQuery<Listing[]>({
		queryKey: ['listings'],
		queryFn: async () => {
			const response = await fetch(`/api/listings`);
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			return response.json().then(res => res.result);
		}
	});
