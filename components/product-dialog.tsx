'use client';
import React from 'react';
import * as Dialog from '@radix-ui/react-dialog';
import { useForm, type SubmitHandler } from 'react-hook-form';
import {
	Box,
	Button,
	Card,
	Flex,
	TextArea,
	TextFieldInput
} from '@radix-ui/themes';

import { type Product } from '@/types';
import { useCreateProductMutation } from '@/hooks/useCreateProductMutation';

import ImageSelector from './image-selector';

const ProductDialog = () => {
	const {
		register,
		handleSubmit,
		control,
		setError,
		reset,
		formState: { errors }
	} = useForm<Product>();
	//TODO resolver is not working

	const [open, setOpen] = React.useState(false);
	const { mutate: postProduct } = useCreateProductMutation();
	const convertFileToBase64 = (file: File) =>
		new Promise((resolve, reject) => {
			const reader = new FileReader();

			reader.onload = () => {
				resolve(reader.result);
			};

			reader.onerror = error => {
				reject(error);
			};

			reader.readAsDataURL(file);
		});

	const onSubmit: SubmitHandler<Product> = async data => {
		const { photo, name, description } = data;

		if (!photo) {
			setError('photo', { message: 'Photo is required' });
			return;
		}

		const base64Photo = await convertFileToBase64(photo);

		postProduct({
			name,
			description,
			photo: base64Photo as string
		});
		setOpen(false);
		reset();
	};

	return (
		<Dialog.Root open={open} onOpenChange={setOpen}>
			<Dialog.Trigger asChild>
				<Button>Add item</Button>
			</Dialog.Trigger>
			<Dialog.Portal>
				<Dialog.Overlay className="fixed inset-0 bg-opacity-5 backdrop-blur-lg dark:bg-black" />
				<Dialog.Content className="fixed left-[50%] top-[50%] w-min min-w-[10vw] translate-x-[-50%] translate-y-[-50%] bg-white  text-black shadow-sm dark:bg-black dark:text-white">
					<form onSubmit={handleSubmit(onSubmit)}>
						<Card className="p-5">
							<Flex className="flex-col " justify="center">
								<Dialog.Title className="text-center text-lg">
									Add a new item
								</Dialog.Title>
								<Flex className=" flex-col gap-2 p-2">
									<TextFieldInput
										type="text"
										{...register('name', { required: true })}
										placeholder="Item name"
										className="m-2 bg-transparent"
									/>
									{errors.name && (
										<p className="text-ruby-red-400">Invalid name</p>
									)}
									<TextArea
										{...register('description', { required: true })}
										placeholder="Item Description"
										rows={3}
										className="bg-transparent p-2"
									/>
									{errors.description && (
										<p className="text-ruby-red-400">Invalid Description</p>
									)}
									<ImageSelector control={control} />
									{errors.photo && (
										<p className="text-ruby-red-400">Invalid Photo</p>
									)}
								</Flex>
							</Flex>
						</Card>
						<Box className="grid w-full grid-cols-2 ">
							<Button
								className=" w-full "
								variant="solid"
								radius="medium"
								color="teal"
								aria-label="Close"
							>
								Save
							</Button>

							<Dialog.Close asChild>
								<Button
									className="w-full"
									variant="solid"
									radius="medium"
									color="ruby"
									type="button"
									aria-label="Close"
								>
									Close
								</Button>
							</Dialog.Close>
						</Box>
					</form>
				</Dialog.Content>
			</Dialog.Portal>
		</Dialog.Root>
	);
};

export default ProductDialog;
