'use client';

import { useState, type ReactNode } from 'react';
import { Theme } from '@radix-ui/themes';
import { ThemeProvider } from 'next-themes';
import { SessionProvider } from 'next-auth/react';
import { QueryClientProvider, QueryClient } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
type ProvidersProps = {
	children: ReactNode;
};

const Providers = ({ children }: ProvidersProps) => {
	const [client] = useState(
		new QueryClient({
			defaultOptions: {
				queries: {
					refetchOnWindowFocus: false,
					staleTime: 300_000,
					gcTime: 300_000
				}
			}
		})
	);
	return (
		<QueryClientProvider client={client}>
			<ThemeProvider
				attribute="class"
				//				forcedTheme="dark"
				defaultTheme="system"
				enableSystem
			>
				<Theme
					accentColor="teal"
					hasBackground={false}
					grayColor="mauve"
					panelBackground="solid"
					scaling="100%"
					radius="medium"
				>
					<SessionProvider>{children}</SessionProvider>
				</Theme>
			</ThemeProvider>
			<ReactQueryDevtools initialIsOpen={false} />
		</QueryClientProvider>
	);
};

export default Providers;
