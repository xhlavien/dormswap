import { Button } from '@radix-ui/themes';
import React from 'react';

import LoadingSpinner from '@/components/loading-spinner';

type FormSubmitButtonProps = {
	label: string;
	isLoading: boolean;
	isError: boolean;
};

export const FormSubmitButton = ({
	label,
	isLoading,
	isError
}: FormSubmitButtonProps) => (
	<div className="mx-auto mt-5">
		{!isLoading ? (
			<Button type="submit">Save Changes</Button>
		) : (
			<LoadingSpinner label={`Updating ${label}..}`} />
		)}
		{isError && <p>{`There was an error updating the ${label}...`}</p>}
	</div>
);
