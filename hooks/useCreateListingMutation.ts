import { useMutation, useQueryClient } from '@tanstack/react-query';
import { type Listing } from '.prisma/client';
import router from 'next/router';

export const useCreateListingMutation = () => {
	const queryClient = useQueryClient();

	return useMutation({
		mutationKey: ['listings'],
		mutationFn: async (listing: Listing) => {
			const response = await fetch('/api/auth/listing', {
				method: 'POST',
				body: JSON.stringify(listing),
				headers: {
					'Content-Type': 'application/json'
				}
			});

			if (!response.ok) {
				throw new Error('Failed to create listing');
			}

			return response.json() as unknown as Listing;
		},
		onSuccess: async () => {
			await queryClient.invalidateQueries({ queryKey: ['products'] });
			await queryClient.invalidateQueries({ queryKey: ['listings'] });
			router.push('/listings');
		}
	});
};
