import React from 'react';
import { Box, Button, Flex, Heading } from '@radix-ui/themes';
import { useRouter } from 'next/navigation';

import { useGetListings } from '@/hooks/useFindAllListingsQuery';
import LoadingSpinner from '@/components/loading-spinner';
import ListingList from '@/components/listing-list/listing-list';

import ProductDialog from './product-dialog';
import ListingDialog from './listing-dialog';

type ListingListContainerProps = {
	userId: string;
};

const ListingListContainer = ({ userId }: ListingListContainerProps) => {
	const {
		data: listings,
		isLoading: isLoadingListings,
		isError: isErrorListings
	} = useGetListings();

	if (isLoadingListings) {
		return <LoadingSpinner label="Loading listings..." />;
	}

	if (!listings || isErrorListings) {
		return <h2 className="text-white">Error loading listings..</h2>;
	}

	return (
		<Flex direction="column" justify="center" align="center">
			<Flex justify="between" align="center">
				<Heading>Listings</Heading>
			</Flex>
			<Box className="my-4">
				<ListingDialog />
			</Box>

			<ListingList listings={listings.filter(l => l.userId === userId)} />
		</Flex>
	);
};

export default ListingListContainer;
