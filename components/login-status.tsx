'use client';

import { signIn, signOut, useSession } from 'next-auth/react';
import { redirect } from 'next/navigation';

import LoadingSpinner from '@/components/loading-spinner';

export const LoginStatus = () => {
	const { data, status } = useSession();
	if (status === 'loading') return <LoadingSpinner label="Checking auth.." />;
	if (status === 'unauthenticated') {
		return (
			<button
				onClick={() => signIn('discord')}
				className="rounded border border-white p-3"
			>
				Sign in with Discord
			</button>
		);
	}
	redirect('/');
};
