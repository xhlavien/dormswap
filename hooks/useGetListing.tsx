import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { type ListingWithProductForExchange } from '@/types';

export const useGetListing = (listingId: string) =>
	useQuery<ListingWithProductForExchange>({
		queryKey: ['listing', listingId],
		queryFn: async () => {
			const response = await fetch(`/api/detail/${listingId}`);
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			return response.json().then(res => {
				console.log(res);
				return res.result;
			});
		}
	});
