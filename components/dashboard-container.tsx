import React from 'react';
import { Box } from '@radix-ui/themes';

import { db } from '@/server/db';
import Searchbar from '@/components/searchbar';

import DashboardList from './dashboard-list';

type DashboardContainerProps = {
	userId: string;
	query: string | undefined;
};

const DashboardContainer = async ({
	userId,
	query
}: DashboardContainerProps) => {
	const listings = await db.listing.findMany();

	return (
		<Box>
			<Searchbar />
			<DashboardList query={query} listings={listings} />
		</Box>
	);
};

export default DashboardContainer;
