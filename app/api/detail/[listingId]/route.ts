import { getServerSession } from 'next-auth';
import { type NextRequest, NextResponse } from 'next/server';

import { authOptions } from '@/server/auth';
import { db } from '@/server/db';

export const GET = async (
	_: NextRequest,
	{ params: { listingId } }: { params: { listingId: string } }
) => {
	const session = await getServerSession(authOptions);

	const id = Number(listingId);

	if (!session?.user?.id)
		return NextResponse.json(
			{ status: 401, error: 'NOT_AUTHORIZED' },
			{ status: 401 }
		);
	if (!listingId)
		return NextResponse.json(
			{ status: 400, error: 'NO_LISTING_ID' },
			{ status: 400 }
		);

	try {
		const result = await db.listing.findUnique({
			where: { listingId: id },
			include: {
				productForExchange: true
			}
		});
		// const product = await db.product.findUnique({
		// 	where: { productId: listing?.offeredProductId }
		// });
		// const result = { ...listing, product };

		return NextResponse.json({ status: 200, result }, { status: 200 });
	} catch (e) {
		return NextResponse.json(
			{ status: 400, error: 'LISTING_NOT_FOUND' },
			{ status: 400 }
		);
	}
};
