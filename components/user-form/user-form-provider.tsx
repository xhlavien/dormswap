'use client';

import React, { type PropsWithChildren, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { type User } from '.prisma/client';
import { useQueryClient } from '@tanstack/react-query';
import { useRouter } from 'next/navigation';
import { v4 } from 'uuid';
import { zodResolver } from '@hookform/resolvers/zod';

import { useUpdateUserMutation } from '@/hooks/useUpdateUserMutation';
import { getUserQueryKey } from '@/hooks/useFindUserByIdQuery';
import { UserSchema } from '@/schemas/user-schema';
import { isProfileValid } from '@/utils/utils';
import { FormSubmitButton } from '@/components/user-form/user-form-submit-button';
import { Text } from '@radix-ui/themes';

type UserFormProviderProps = PropsWithChildren<{
	userData: User;
}>;

export const UserFormProvider = ({
	children,
	userData
}: UserFormProviderProps) => {
	const methods = useForm<User>({ resolver: zodResolver(UserSchema) });

	const queryClient = useQueryClient();

	const router = useRouter();

	const [imageError, setImageError] = useState<boolean>(false);

	const {
		mutate: updateUser,
		isError: isUserUpdateError,
		isPending: isUserUpdatePending
	} = useUpdateUserMutation();

	const onSubmit = (user: User) => {
		console.log(user);
		updateUser(
			{ ...user, id: userData.id },
			{
				onSuccess: () => {
					queryClient.removeQueries({ queryKey: getUserQueryKey(user.id) });
					const randomId = v4();
					router.push(`/profile?lastUpdated=${randomId}`);
					router.refresh();
				}
			}
		);
	};

	return (
		<FormProvider {...methods}>
			<form onSubmit={methods.handleSubmit(onSubmit)}>
				<div className="flex flex-col gap-5">
					<Text>
						If you got redirected here from other pages, you need to fill in the
						required profile details.
					</Text>
					<div className="flex w-full items-center justify-between gap-20">
						{userData.image ? (
							!imageError ? (
								<img
									src={userData.image}
									alt="user-profile-img"
									onError={() => {
										setImageError(true);
									}}
									className="w-24 text-white"
								/>
							) : (
								<div className="flex h-24 w-24 items-center justify-center border text-white">
									<p className="text-center">Image URL not found</p>
								</div>
							)
						) : (
							<div className="flex h-24 w-24 items-center justify-center border text-white">
								<p className="text-center">No Image Selected</p>
							</div>
						)}
						<div>
							{isProfileValid(userData) ? (
								<p className="text-green-700">Profile completed!</p>
							) : (
								<p className="text-red-700">
									Profile incomplete, please set the required fields.
								</p>
							)}
						</div>
					</div>
					{children}
					<FormSubmitButton
						label="user"
						isLoading={isUserUpdatePending}
						isError={isUserUpdateError}
					/>
				</div>
			</form>
		</FormProvider>
	);
};
