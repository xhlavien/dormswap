'use client';
import React from 'react';
import { Box, Button, Card, Container, Flex, Heading } from '@radix-ui/themes';
import Image from 'next/image';
import { useQueryClient } from '@tanstack/react-query';

import { useGetProducts } from '@/hooks/useGetProducts';
import { useDeleteProductMutation } from '@/hooks/useDeleteProductMutation';
import { type Product } from '@/types';
import ProductDialog from '@/components/product-dialog';

const Inventory = () => {
	const { data, isLoading } = useGetProducts();

	const { mutate: deleteProduct } = useDeleteProductMutation();

	if (isLoading) {
		return <Box>Loading...</Box>;
	}

	if (!data) {
		return <h2>There are no products in your inventory </h2>;
	}

	return (
		<Container>
			<Flex justify="center" direction="column" align="center">
				<Heading>Inventory</Heading>
				<Box className="my-4">
					<ProductDialog />
				</Box>
				<Flex
					justify="center"
					direction="column"
					align="center"
					gap="5"
					className="my-5"
				>
					{data.map((product: Product) => (
						<Box key={product.productId}>
							<Card className="max-w-[600px] p-6  ">
								<p className="py-2 text-xl font-bold">{product.name}</p>

								<Image
									className=" h-auto max-h-[75%]  w-full rounded-md object-cover shadow-md"
									src={product.photo}
									alt={product.name}
									width={1000}
									height={1000}
								/>

								<p className="my-2">{product.description}</p>
							</Card>

							<Button
								radius="none"
								color="red"
								className="w-full "
								onClick={() => deleteProduct(product.productId ?? NaN)}
							>
								Delete
							</Button>
						</Box>
					))}
				</Flex>
			</Flex>
		</Container>
	);
};

export default Inventory;
