import { redirect } from 'next/navigation';
import { type PropsWithChildren } from 'react';

import { db } from '@/server/db';
import { getServerAuthSession } from '@/server/auth';
import { isProfileValid } from '@/utils/utils';

const ProfileCheck = async ({ children }: PropsWithChildren) => {
	const status = await getServerAuthSession();
	if (!status) {
		// User unauthenticated, redirect to login
		redirect('/login');
	}

	const user = await db.user.findUnique({ where: { id: status.user.id } });

	console.log(user);

	if (!user || !isProfileValid(user)) {
		console.log('redirecting...');
		redirect('/profile');
	}

	return <div>{children}</div>;
};

export default ProfileCheck;
