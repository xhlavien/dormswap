import { type NextRequest, NextResponse } from 'next/server';
import { getServerSession } from 'next-auth';

import { db } from '@/server/db';
import { authOptions } from '@/server/auth';

export const DELETE = async (
	_: NextRequest,
	{ params: { listingId } }: { params: { listingId: string } }
) => {
	const session = await getServerSession(authOptions);
	const id = parseFloat(listingId);
	if (!session?.user?.id)
		return NextResponse.json(
			{ status: 401, error: 'NOT_AUTHORIZED' },
			{ status: 401 }
		);
	try {
		const result = await db.listing.delete({
			where: { listingId: id }
		});

		return NextResponse.json({ status: 200, result }, { status: 200 });
	} catch (e) {
		return NextResponse.json(
			{ status: 400, error: 'LISITNG_NOT_DELETED' },
			{ status: 400 }
		);
	}
};
