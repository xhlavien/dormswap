'use client';

import * as Select from '@radix-ui/react-select';
import { ChevronDownIcon } from '@radix-ui/react-icons';
import { Button, Flex, SelectItem } from '@radix-ui/themes';
import React, { useState } from 'react';
import { useRouter } from 'next/navigation';
import { type Listing } from '.prisma/client';
import { useQueryClient } from '@tanstack/react-query';

import { useGetProducts } from '@/hooks/useGetProducts';
import LoadingSpinner from '@/components/loading-spinner';
import { useOfferProductForTradeMutation } from '@/hooks/useOfferProductForTradeMutation';

type OfferItemProps = {
	listing: Listing;
};

const OfferItem = ({ listing }: OfferItemProps) => {
	const {
		data: products,
		isLoading: isLoadingProducts,
		isError: isErrorProducts
	} = useGetProducts();

	const [selectedProductId, setSelectedProductId] = useState<
		number | undefined
	>(undefined);

	const { mutate: offerProductForExchange } = useOfferProductForTradeMutation();

	const router = useRouter();

	const queryClient = useQueryClient();

	if (isLoadingProducts) {
		return <LoadingSpinner label="Loading products..." />;
	}

	if (!products || isErrorProducts) {
		return <h2>Products error</h2>;
	}

	const productsToOffer = products.filter(p => p.offeredInListing === null);

	const onSubmit = () => {
		if (selectedProductId !== undefined) {
			offerProductForExchange(
				{
					...listing,
					offeredProductId: selectedProductId
				},
				{
					onSuccess: () => {
						console.log('success offer product');
						queryClient.removeQueries({ queryKey: ['products'] });
						queryClient.removeQueries({
							queryKey: ['product', selectedProductId]
						});
						queryClient.removeQueries({
							queryKey: ['listing', listing.listingId]
						});
						router.push('/dashboard');
					}
				}
			);
		}
	};

	return (
		<Flex direction="column" gap="4" justify="center" align="center">
			<Select.Root
				onValueChange={value => {
					setSelectedProductId(Number(value));
				}}
				value={selectedProductId?.toString() ?? undefined}
				disabled={productsToOffer.length < 1}
			>
				{productsToOffer.length < 1 ? (
					<Select.Trigger
						className="flex w-full cursor-pointer items-center justify-between gap-2 rounded bg-ruby-red-500 p-2 shadow-md hover:bg-teal-500 focus:outline-none focus:ring-2 focus:ring-black"
						aria-label="Food"
						color="ruby"
					>
						No item to exchange
					</Select.Trigger>
				) : (
					<Select.Trigger
						className="flex w-full cursor-pointer items-center justify-between gap-2 rounded bg-teal-500 p-2 shadow-md hover:bg-teal-500 focus:outline-none focus:ring-2 focus:ring-black"
						aria-label="Food"
					>
						<Select.Value
							className="flex-1 overflow-hidden"
							placeholder="Select item for exchange..."
						/>
						<Select.Icon>
							<ChevronDownIcon />
						</Select.Icon>
					</Select.Trigger>
				)}
				<Select.Portal>
					<Select.Content className="overflow-hidden rounded bg-teal-500 shadow-lg dark:text-white">
						<Select.Viewport className="relative flex max-h-48 flex-col overflow-y-auto p-2">
							{productsToOffer.map(p => (
								<SelectItem
									className=" p-2 text-white"
									value={p.productId.toString()}
									key={p.productId}
								>
									{p.name}
								</SelectItem>
							))}
						</Select.Viewport>
					</Select.Content>
				</Select.Portal>
			</Select.Root>
			<Button size="3" onClick={onSubmit}>
				Offer product
			</Button>
		</Flex>
	);
};

export default OfferItem;
