import { type Listing } from '.prisma/client';
import { NextResponse, type NextRequest } from 'next/server';
import { getServerSession } from 'next-auth';

import { authOptions } from '@/server/auth';
import { db } from '@/server/db';

export const POST = async (request: Request) => {
	const session = await getServerSession(authOptions);
	const listingBody: Listing = await request.json();

	if (!session?.user?.id)
		return NextResponse.json(
			{ status: 401, error: 'NOT_AUTHORIZED' },
			{ status: 401 }
		);

	const createdListing = await db.listing.create({
		data: {
			created: new Date(),
			isOpen: true,
			description: listingBody.description,
			user: {
				connect: { id: session.user.id }
			},
			offeredProduct: {
				connect: { productId: listingBody.offeredProductId }
			}
		}
	});

	return Response.json(createdListing);
};

export const PATCH = async (request: NextRequest) => {
	const listingBody: Listing & { offeredProductId: number } =
		await request.json();

	if (!listingBody.offeredProductId) {
		console.log('No offered product id provided');
		return Response.error();
	}

	const productForExchange = await db.product.findUnique({
		where: {
			productId: listingBody.offeredProductId
		}
	});

	if (!productForExchange) {
		console.log(
			`Product with id: ${listingBody.offeredProductId} not found in the database`
		);
		return Response.error();
	}

	const updatedListing = await db.listing.update({
		where: {
			listingId: listingBody.listingId
		},
		data: {
			productForExchangeId: listingBody.offeredProductId
		}
	});

	return Response.json(updatedListing);
};
