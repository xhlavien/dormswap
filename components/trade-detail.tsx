'use client';
import {
	Box,
	Button,
	Card,
	Flex,
	Avatar,
	Text,
	Heading
} from '@radix-ui/themes';
import Image from 'next/image';
import React from 'react';
import { type Listing } from '.prisma/client';
import { TbArrowsExchange } from 'react-icons/tb';
import { useRouter } from 'next/navigation';
import { useQueryClient } from '@tanstack/react-query';

import {
	getProductQueryKey,
	useGetProductByIdQuery
} from '@/hooks/useGetProductByIdQuery';
import LoadingSpinner from '@/components/loading-spinner';
import { useResolveTradeMutation } from '@/hooks/useResolveTradeMutation';
import { useFindUserByIdQuery } from '@/hooks/useFindUserByIdQuery';
import { useGetTradeInfoQuery } from '@/hooks/useGetTradeInfoQuery';

type TradeDetailProps = {
	listing: Listing;
	productForExchangeId: number;
	userId: string;
};

const TradeDetail = ({
	listing,
	productForExchangeId,
	userId
}: TradeDetailProps) => {
	const {
		data: offeredProduct,
		isLoading: isLoadingOfferedProduct,
		isError: isErrorOfferedProduct
	} = useGetProductByIdQuery(listing.offeredProductId);

	const {
		data: productForExchange,
		isLoading: isLoadingProductForExchange,
		isError: isErrorProductForExchange
	} = useGetProductByIdQuery(productForExchangeId);

	const { data: trade } = useGetTradeInfoQuery(listing.listingId);
	console.log(trade);
	const { mutate: resolveTrade } = useResolveTradeMutation();

	const router = useRouter();

	const queryClient = useQueryClient();

	if (isLoadingProductForExchange || isLoadingOfferedProduct) {
		return <LoadingSpinner label="Loading products in trade..." />;
	}

	if (
		isErrorOfferedProduct ||
		isErrorProductForExchange ||
		!offeredProduct ||
		!productForExchange
	) {
		return <h2>Error loading products in trade.</h2>;
	}

	const onSubmit = (resolve: boolean, declined: boolean) => {
		const secondUserId =
			listing.userId === userId ? productForExchange.ownerId : userId;
		resolveTrade(
			{ ...listing, resolve, secondUserId },
			{
				onSuccess: () => {
					console.log('success resolve trade');
					if (resolve) {
						queryClient.removeQueries({ queryKey: ['products'] });
						router.push('/inventory');
					} else {
						if (declined) {
							queryClient.removeQueries({ queryKey: ['listings'] });
							router.push('/listings');
						} else {
							queryClient.removeQueries({
								queryKey: ['listings']
							});
							queryClient.removeQueries({
								queryKey: ['product']
							});
							router.push('/dashboard');
						}
					}
				}
			}
		);
	};

	return (
		<Flex direction="column" gap="3" className="max-w-[1000px]">
			<Flex align="center">
				<Box key={offeredProduct.productId}>
					<Card className="max-w-[400px] rounded-md bg-white p-6 shadow-md">
						<Flex direction="column" gap="4">
							{trade?.owner && (
								<Flex align="center" gap="2">
									<Avatar
										src={trade?.owner.image ?? undefined}
										alt={trade?.owner.name ?? undefined}
										size="3"
										fallback=""
									/>
									<Text className="text-black">{trade?.owner.name}</Text>
								</Flex>
							)}

							<Heading
								as="h1"
								size="4"
								className="mb-2 text-3xl font-bold text-black"
							>
								{offeredProduct.name}
							</Heading>

							<Text
								size="3"
								className="mb-4 h-[70px] overflow-auto text-gray-500"
							>
								{offeredProduct.description}
							</Text>
							<Image
								className="h-auto max-h-[75%] w-full rounded-md object-cover shadow-md"
								src={offeredProduct.photo}
								alt={offeredProduct.name}
								width={1000}
								height={1000}
							/>
						</Flex>
					</Card>
				</Box>
				<TbArrowsExchange className="text-6xl" />
				<Box key={productForExchange.productId}>
					<Card className="max-w-[400px] rounded-md bg-white p-6 shadow-md">
						<Flex direction="column" gap="4">
							{trade?.trader && (
								<Flex align="center" gap="2">
									<Avatar
										src={trade?.trader.image ?? undefined}
										alt={trade?.trader.name ?? undefined}
										size="3"
										fallback=""
									/>
									<Text className="text-black">{trade?.trader.name}</Text>
								</Flex>
							)}

							<Heading
								as="h1"
								size="4"
								className="mb-2 text-3xl font-bold text-black"
							>
								{productForExchange.name}
							</Heading>

							<Text
								size="3"
								className="mb-4 h-[70px] overflow-auto text-gray-500"
							>
								{productForExchange.description}
							</Text>

							<Image
								className="h-auto max-h-[75%] w-full rounded-md object-cover shadow-md"
								src={productForExchange.photo}
								alt={productForExchange.name}
								width={1000}
								height={1000}
							/>
						</Flex>
					</Card>
				</Box>
			</Flex>
			{listing.userId === userId ? (
				<Flex>
					<Button
						radius="none"
						style={{ flex: 1 }}
						onClick={() => onSubmit(true, false)}
					>
						Trade
					</Button>
					<Button
						color="ruby"
						radius="none"
						style={{ flex: 1 }}
						onClick={() => onSubmit(false, true)}
					>
						Decline
					</Button>
				</Flex>
			) : (
				<Button
					color="ruby"
					radius="none"
					onClick={() => onSubmit(false, false)}
				>
					Cancel offer
				</Button>
			)}
		</Flex>
	);
};

export default TradeDetail;
