import { type NextRequest, NextResponse } from 'next/server';
import { getServerSession } from 'next-auth';

import { db } from '@/server/db';
import { authOptions } from '@/server/auth';

export const GET = async (
	request: Request,
	{ params: { productId } }: { params: { productId: string } }
) => {
	const id = parseFloat(productId);
	const product = await db.product.findUnique({
		where: {
			productId: id
		}
	});

	console.log(product);

	if (!product) {
		return NextResponse.error();
	}

	return NextResponse.json({ status: 200, result: product });
};

export const DELETE = async (
	req: NextRequest,
	{ params: { productId } }: { params: { productId: string } }
) => {
	const session = await getServerSession(authOptions);
	const id = parseFloat(productId);
	if (!session?.user?.id)
		return NextResponse.json(
			{ status: 401, error: 'NOT_AUTHORIZED' },
			{ status: 401 }
		);
	try {
		const result = await db.product.delete({
			where: { productId: id }
		});

		return NextResponse.json({ status: 200, result }, { status: 200 });
	} catch (e) {
		return NextResponse.json(
			{ status: 400, error: 'PRODUCT_NOT_CREATED' },
			{ status: 400 }
		);
	}
};
