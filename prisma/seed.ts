import { PrismaClient } from './generated/client';
// import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();
const main = async () => {
	const klacelova = await prisma.dorm.upsert({
		where: { dormId: 1 },
		update: {},
		create: {
			dormId: 1,
			name: 'Koleje Klácelova'
		}
	});

	const manesova = await prisma.dorm.upsert({
		where: { dormId: 2 },
		update: {},
		create: {
			dormId: 2,
			name: 'Koleje Mánesova'
		}
	});

	const kounicova = await prisma.dorm.upsert({
		where: { dormId: 3 },
		update: {},
		create: {
			dormId: 3,
			name: 'Koleje Kounicova'
		}
	});
};
main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async e => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
