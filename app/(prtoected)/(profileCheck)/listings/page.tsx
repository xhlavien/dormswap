'use client';
import { useSession } from 'next-auth/react';
import React from 'react';

import LoadingSpinner from '@/components/loading-spinner';
import ListingListContainer from '@/components/listing-list-container';

const ListingPage = () => {
	const { data, status } = useSession();

	if (!data?.user) {
		return <LoadingSpinner label="Loading data..." />;
	}

	return <ListingListContainer userId={data.user.id} />;
};

export default ListingPage;
