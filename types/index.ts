import { z } from 'zod';
import { type Prisma } from '.prisma/client';

export const DormSchema = z.object({
	dormId: z.number(),
	name: z.string()
});

export const UserSchema = z.object({
	id: z.string(),
	name: z.string().optional(),
	firstName: z.string().optional(),
	lastName: z.string().optional(),
	image: z.string().optional(),
	email: z.string().optional(),
	emailVerified: z.date().optional(),
	phoneNumber: z.string().optional(),
	password: z.string().optional(),
	dormId: z.number().optional()
});

export const ListingSchema = z.object({
	listingId: z.number(),
	userId: z.string(),
	created: z.date(),
	description: z.string().max(500),
	offeredProductId: z.number(),
	productForExchangeId: z.number(),
	isOpen: z.boolean()
});

type ProductWithOfferedInListing = Prisma.ProductGetPayload<{
	include: { offeredInListing: true };
}>;

type ListingWithProductForExchange = Prisma.ListingGetPayload<{
	include: { productForExchange: true };
}>;

export const ProductSchema = z.object({
	productId: z.number().optional(),
	name: z.string().max(100),
	description: z.string().max(500),
	photo: z.string(),
	ownerId: z.string().optional()
});

export const AccountSchema = z.object({
	id: z.string(),
	userId: z.string(),
	type: z.string(),
	provider: z.string(),
	providerAccountId: z.string(),
	refresh_token: z.string().optional(),
	access_token: z.string().optional(),
	expires_at: z.number().optional(),
	token_type: z.string().optional(),
	scope: z.string().optional(),
	id_token: z.string().optional(),
	session_state: z.string().optional()
});

export const SessionSchema = z.object({
	id: z.string(),
	sessionToken: z.string(),
	userId: z.string(),
	expires: z.date()
});

export const VerificationTokenSchema = z.object({
	identifier: z.string(),
	token: z.string(),
	expires: z.date()
});

type Dorm = z.infer<typeof DormSchema>;
type User = z.infer<typeof UserSchema>;
type Listing = z.infer<typeof ListingSchema>;
type Product = z.infer<typeof ProductSchema>;
type Account = z.infer<typeof AccountSchema>;
type Session = z.infer<typeof SessionSchema>;
type VerificationToken = z.infer<typeof VerificationTokenSchema>;

export type {
	Dorm,
	User,
	Listing,
	ProductWithOfferedInListing,
	ListingWithProductForExchange,
	Product,
	Account,
	Session,
	VerificationToken
};
