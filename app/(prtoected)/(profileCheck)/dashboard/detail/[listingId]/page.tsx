import { useSession } from 'next-auth/react';
import React from 'react';
import { redirect } from 'next/navigation';

import ListingDetailContainer from '@/components/listing-detail-container';
import { getServerAuthSession } from '@/server/auth';
import { db } from '@/server/db';

const DetailPage = async ({ params }: { params: { listingId: string } }) => {
	const status = await getServerAuthSession();
	if (!status) {
		// User unauthenticated, redirect to login
		redirect('/login');
	}

	const user = await db.user.findUnique({ where: { id: status.user.id } });

	if (!user) {
		redirect('/login');
	}
	return (
		<ListingDetailContainer userId={user.id} listingId={params.listingId} />
	);
};

export default DetailPage;
