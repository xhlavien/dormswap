'use client';
import * as Label from '@radix-ui/react-label';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { type User } from '.prisma/client';

type TextFieldProps = {
	id: 'image' | 'firstName' | 'lastName' | 'email' | 'phoneNumber';
	label: string;
	value: string;
	placeholder: string;
};

export const TextField = ({
	id,
	label,
	value,
	placeholder
}: TextFieldProps) => {
	const { register, formState } = useFormContext<User>();

	const validationError = formState.errors[id];

	return (
		<div>
			<div className="flex w-full justify-between items-center gap-20">
				<Label.Root
					className="text-base font-medium"
					htmlFor={id}
				>
					{label}
				</Label.Root>
				<input
					className="w-50 inline-flex h-9 items-center justify-center rounded bg-black px-2.5 text-base leading-none text-white focus:outline-none focus:ring-2 focus:ring-black"
					id={id}
					{...register(id, { required: true })}
					aria-invalid={validationError ? 'true' : 'false'}
					placeholder={placeholder}
					defaultValue={value}
				/>
			</div>
			<div className="flex justify-end">
				{validationError && (
					<p className="text-red-700">{validationError.message}</p>
				)}
			</div>
		</div>
	);
};
