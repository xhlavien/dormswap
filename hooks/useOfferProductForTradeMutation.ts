import { useMutation } from '@tanstack/react-query';
import { type Listing, type User } from '.prisma/client';

export const useOfferProductForTradeMutation = () =>
	useMutation({
		mutationFn: (listing: Listing & { offeredProductId: number }) =>
			fetch(`/api/auth/listing?offeredProductId=${listing.offeredProductId}`, {
				method: 'PATCH',
				body: JSON.stringify(listing)
			})
				.then(res => res.json())
				.then(res => res as User)
	});
