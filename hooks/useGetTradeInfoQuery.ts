import { useQuery } from '@tanstack/react-query';

import {
	type Listing,
	type User,
	type ProductWithOfferedInListing
} from '@/types';

export const useGetTradeInfoQuery = (id: number) =>
	useQuery<{ listing: Listing; owner: User; trader: User }>({
		queryKey: ['tradeInfo', id],
		queryFn: async () => {
			const response = await fetch(`/api/auth/listing/${id}/trade`);
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			return response.json().then(res => res.result);
		}
	});
