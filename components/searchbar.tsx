'use client';
import React, { useEffect, useState } from 'react';
import { TextField } from '@radix-ui/themes';
import { MagnifyingGlassIcon } from '@radix-ui/react-icons';
import { useRouter } from 'next/navigation';
import { useDebounce } from 'use-debounce';

const Searchbar = () => {
	const [searchQuery, setSearchQuery] = useState('');
	const router = useRouter();
	const [query] = useDebounce(searchQuery, 500);

	useEffect(() => {
		if (query) {
			router.push(`/dashboard?query=${query}`);
		} else {
			router.push(`/dashboard`);
		}
	}, [query, router]);

	return (
		<TextField.Root className="mx-auto mb-4 w-96">
			<TextField.Slot>
				<MagnifyingGlassIcon height="16" width="16" />
			</TextField.Slot>
			<TextField.Input
				value={searchQuery}
				placeholder="Search for items..."
				onChange={e => setSearchQuery(e.target.value)}
			/>
		</TextField.Root>
	);
};

export default Searchbar;
