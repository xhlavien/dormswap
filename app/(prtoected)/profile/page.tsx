import React from 'react';
import { redirect } from 'next/navigation';

import { db } from '@/server/db';
import { getServerAuthSession } from '@/server/auth';
import { UserFormProvider } from '@/components/user-form/user-form-provider';
import { TextField } from '@/components/user-form/text-field';
import { SelectField } from '@/components/user-form/select-field';

const Profile = async () => {
	const status = await getServerAuthSession();
	if (!status) {
		// User unauthenticated, redirect to login
		redirect('/login');
	}

	const user = await db.user.findUnique({ where: { id: status.user.id } });

	const dorms = await db.dorm.findMany();

	if (!user) {
		console.log('User not found in profile page. wtf?');
		redirect('/login');
	}

	return (
		<UserFormProvider userData={user}>
			<TextField
				id="image"
				label="Image"
				value={user.image ?? ''}
				placeholder="Paste URL to your image"
			/>
			<TextField
				id="firstName"
				label="First name"
				value={user.firstName ?? ''}
				placeholder="Enter your first name"
			/>
			<TextField
				id="lastName"
				label="Last name"
				value={user.lastName ?? ''}
				placeholder="Enter your last name"
			/>
			<TextField
				id="email"
				label="E-mail"
				value={user.email ?? ''}
				placeholder="Enter your email"
			/>
			<TextField
				id="phoneNumber"
				label="Phone"
				value={user.phoneNumber ?? ''}
				placeholder="Enter your phone number"
			/>
			<SelectField
				values={dorms.map(d => [d.dormId, d.name])}
				value={user.dormId?.toString()}
			/>
		</UserFormProvider>
	);
};

export default Profile;
