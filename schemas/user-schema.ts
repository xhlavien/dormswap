import { z } from 'zod';

export const UserSchema = z.object({
	firstName: z.string().min(1).max(20),
	lastName: z.string().min(1).max(20),
	email: z.string().min(1).email(),
	phoneNumber: z.string().min(9),
	dormId: z.number(),
	image: z.string().min(0)
});
