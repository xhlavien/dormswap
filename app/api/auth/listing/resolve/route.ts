import { type NextRequest } from 'next/server';
import { type Listing } from '.prisma/client';

import { db } from '@/server/db';

export const PATCH = async (request: NextRequest) => {
	// user

	console.log('CANCEL OFFER SHOULD GET HERE 1');

	const listingBody: Listing & { resolve: boolean; secondUserId: string } =
		await request.json();

	console.log(listingBody);

	if (!listingBody.offeredProductId) {
		console.log('No offered product id provided');
		return Response.error();
	}

	if (!listingBody.productForExchangeId) {
		console.log('No exchange product id provided');
		return Response.error();
	}

	const offeringProduct = await db.product.findUnique({
		where: {
			productId: listingBody.offeredProductId
		}
	});

	const productForExchange = await db.product.findUnique({
		where: {
			productId: listingBody.productForExchangeId
		}
	});

	if (!offeringProduct) {
		console.log(
			`Product with id: ${listingBody.offeredProductId} not found in the databse`
		);
		return Response.error();
	}

	if (!productForExchange) {
		console.log(
			`Product with id: ${listingBody.productForExchangeId} not found in the databse`
		);
		return Response.error();
	}

	console.log('CANCEL OFFER SHOULD GET HERE 2');

	if (listingBody.resolve) {
		const updatedListing = await db.listing.delete({
			where: {
				listingId: listingBody.listingId
			}
		});

		const updatedProductForExchange = await db.product.update({
			where: {
				productId: productForExchange.productId
			},
			data: {
				ownerId: offeringProduct.ownerId
			}
		});

		const updatedOfferingProduct = await db.product.update({
			where: {
				productId: offeringProduct.productId
			},
			data: {
				ownerId: productForExchange.ownerId
			}
		});
	} else {
		console.log('CANCEL OFFER SHOULD GET HERE 3');
		const updatedListing = await db.listing.update({
			where: {
				listingId: listingBody.listingId
			},
			data: {
				productForExchangeId: null
			}
		});
	}

	return Response.json({ status: 200 });
};
