import React from 'react';

import { type Product, type Listing } from '@/types';

import DashboardItem from './dashboard-item';

type ListingItemContainerProps = {
	listing?: Listing;
	product: Product;
};

const DashboardItemContainer = async ({
	listing,
	product
}: ListingItemContainerProps) => {
	if (!listing) {
		throw new Error('Listing not found');
	}

	return (
		<div className="flex flex-col">
			<DashboardItem listing={listing} product={product} />
		</div>
	);
};

export default DashboardItemContainer;
