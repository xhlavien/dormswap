import { useMutation, useQueryClient } from '@tanstack/react-query';
import router from 'next/router';

import { type Listing } from '@/types';

export const useDeleteListingMutation = () => {
	const queryClient = useQueryClient();

	return useMutation({
		mutationKey: ['listings'],
		mutationFn: async (listingId: number) => {
			await fetch(`api/auth/listing/${listingId}`, {
				method: 'DELETE',
				body: JSON.stringify(listingId)
			})
				.then(res => res.json())
				.then(res => res as Listing);
		},
		onMutate: async (listingId: number) => {
			const previousListings = queryClient.getQueryData<Listing[]>([
				'listings'
			]);
			queryClient.setQueryData<Listing[]>(['listings'], old =>
				old?.filter((listing: Listing) => listing.listingId !== listingId)
			);
			return { previousListings };
		},
		onError: async (err, _listingId, context) => {
			console.log(err);
			queryClient.setQueryData<Listing[]>(
				['listings'],
				context?.previousListings
			);
		},
		onSettled: async () => {
			await queryClient.invalidateQueries({ queryKey: ['products'] });
			await queryClient.invalidateQueries({ queryKey: ['listings'] });
			router.push('/listings');
		}
	});
};
