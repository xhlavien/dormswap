import { useQuery } from '@tanstack/react-query';

import { type ProductWithOfferedInListing } from '@/types';

export const useGetProducts = () =>
	useQuery<ProductWithOfferedInListing[]>({
		queryKey: ['products'],
		queryFn: async () => {
			const response = await fetch(`/api/auth/products`);
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			return response.json().then(res => res.result);
		}
	});
