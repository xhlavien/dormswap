import React from 'react';
import { type Listing } from '.prisma/client';
import { Heading } from '@radix-ui/themes';

import ListingItemContainer from '@/components/listing-item-container';

type ListingListProps = {
	listings: Listing[];
};

const ListingList = ({ listings }: ListingListProps) => (
	<div>
		{listings.length < 1 ? (
			<Heading>No listings available...</Heading>
		) : (
			<div className="flex flex-wrap justify-center gap-12">
				{listings.map(l => (
					<ListingItemContainer key={l.listingId} listing={l} />
				))}
			</div>
		)}
	</div>
);

export default ListingList;
