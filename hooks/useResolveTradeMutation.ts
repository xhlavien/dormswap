import { useMutation } from '@tanstack/react-query';
import { type Listing, type User } from '.prisma/client';

export const useResolveTradeMutation = () =>
	useMutation({
		mutationFn: (
			listing: Listing & { resolve: boolean; secondUserId: string }
		) =>
			fetch(`/api/auth/listing/resolve`, {
				method: 'PATCH',
				body: JSON.stringify(listing)
			})
				.then(res => res.json())
				.then(res => res)
	});
