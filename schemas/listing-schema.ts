import { z } from 'zod';

export const ListingSchema = z.object({
	description: z.string().min(1).max(200),
	offeredProductId: z.number()
});
