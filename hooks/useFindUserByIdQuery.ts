import { type User } from '.prisma/client';
import { useQuery } from '@tanstack/react-query';

export const getUserQueryKey = (id: string) => ['user', id];

export const useFindUserByIdQuery = (id: string) =>
	useQuery<User>({
		queryKey: getUserQueryKey(id),
		queryFn: () =>
			fetch(`/api/auth/user/${id}`, {
				method: 'GET'
			})
				.then(res => res.json())
				.then(res => res.body)
				.catch(err => err)
	});
