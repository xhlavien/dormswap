import { type Dorm } from '.prisma/client';
import { useQuery } from '@tanstack/react-query';

export const getDormsQueryKey = () => ['dorms'];

export const useFindAllDormsQuery = () =>
	useQuery<Dorm[]>({
		queryKey: getDormsQueryKey(),
		queryFn: () =>
			fetch(`api/auth/dorm`, {
				method: 'GET'
			})
				.then(res => res.json())
				.then(res => res.body)
				.catch(err => err)
	});
