'use client';
import { Controller, useFormContext } from 'react-hook-form';
import * as Select from '@radix-ui/react-select';
import { ChevronDownIcon, ChevronUpIcon } from '@radix-ui/react-icons';
import { SelectItem } from '@radix-ui/themes';
import * as Label from '@radix-ui/react-label';
import React from 'react';

type SelectFieldProps = {
	values: [id: number, value: string][];
	value: string | undefined;
};

export const SelectField = ({ values, value }: SelectFieldProps) => {
	const { control, register, setValue, clearErrors, getValues, formState } =
		useFormContext();

	const validationError = formState.errors.dormId;

	const selected = getValues('dormId')?.toString() ?? value;

	return (
		<div>
			<div className="flex w-full justify-between gap-20">
				<Label.Root
					className="text-base font-medium leading-9"
					htmlFor="dorm"
				>
					Selected Dorm *
				</Label.Root>
				<Controller
					defaultValue={selected}
					control={control}
					{...register('dormId')}
					render={({ field }) => (
						<Select.Root
							onValueChange={value => {
								setValue('dormId', Number(value));
								clearErrors();
							}}
							{...field}
							value={selected}
						>
							<Select.Trigger
								className="w-50  inline-flex  cursor-pointer items-center justify-center gap-2 rounded bg-teal-500 px-2.5 text-sm  leading-none text-black shadow-md hover:bg-teal-500 focus:outline-none focus:ring-2 focus:ring-black dark:text-white"
								aria-label="Food"
							>
								<Select.Value placeholder="Select your dorm" />
								<Select.Icon className="SelectIcon">
									<ChevronDownIcon />
								</Select.Icon>
							</Select.Trigger>
							<Select.Portal>
								<Select.Content className="overflow-hidden rounded-lg bg-teal-500 text-black shadow-lg dark:text-white">
									<Select.ScrollUpButton className="SelectScrollButton">
										<ChevronUpIcon />
									</Select.ScrollUpButton>
									<Select.Viewport className="p-2">
										{values.map(d => (
											<SelectItem
												className="w-50 relative m-2 flex cursor-pointer select-none items-center rounded  py-2 text-sm leading-none"
												value={d[0].toString()}
												key={d[0].toString()}
											>
												{d[1]}
											</SelectItem>
										))}
									</Select.Viewport>
									<Select.ScrollDownButton className="SelectScrollButton">
										<ChevronDownIcon />
									</Select.ScrollDownButton>
								</Select.Content>
							</Select.Portal>
						</Select.Root>
					)}
				/>
			</div>
			<div className="flex justify-end">
				{validationError && <p className="text-red-700">Please choose your dorm</p>}
			</div>
		</div>
	);
};
