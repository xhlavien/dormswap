import { type Product } from '.prisma/client';
import { useQuery } from '@tanstack/react-query';

export const getProductQueryKey = (id: number) => ['product', id];

export const useGetProductByIdQuery = (id: number) =>
	useQuery<Product>({
		queryKey: getProductQueryKey(id),
		queryFn: () =>
			fetch(`/api/auth/products/${id}`, {
				method: 'GET'
			})
				.then(res => res.json())
				.then(res => res.result)
				.catch(err => err)
	});
