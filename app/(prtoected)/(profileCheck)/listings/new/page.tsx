'use client';
import { useSession } from 'next-auth/react';
import React from 'react';

import LoadingSpinner from '@/components/loading-spinner';
import ListingDialog from '@/components/listing-dialog';

const CreateListingPage = () => {
	const { data, status } = useSession();

	if (status === 'loading') {
		return <LoadingSpinner label="Loading profile..." />;
	}

	if (!data?.user) {
		return <h2 className="text-white">Error loading user...</h2>;
	}

	return <ListingDialog />;
};

export default CreateListingPage;
