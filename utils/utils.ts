import { type User } from '.prisma/client';

export const isProfileValid = (user?: User) =>
	user?.firstName &&
	user?.lastName &&
	user?.email &&
	user?.phoneNumber &&
	user?.dormId;
