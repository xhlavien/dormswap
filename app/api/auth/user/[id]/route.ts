import { type User } from '.prisma/client';
import { NextResponse } from 'next/server';

import { db } from '@/server/db';

export const GET = async (
	request: Request,
	{ params }: { params: { id: string } }
) => {
	const id = params.id;

	const user = await db.user.findUnique({
		where: {
			id
		}
	});

	if (!user) {
		return NextResponse.error();
	}

	return NextResponse.json({ status: 200, body: user });
};

export const PATCH = async (
	request: Request,
	{ params }: { params: { id: string } }
) => {
	const id = params.id;

	const userBody: User = await request.json();

	console.log(id);

	console.log(userBody);

	if (!id) {
		console.log('No ID provided');
		return Response.error();
	}

	const updatedUser = await db.user.update({
		where: {
			id
		},
		data: {
			firstName: userBody.firstName,
			lastName: userBody.lastName,
			phoneNumber: userBody.phoneNumber,
			email: userBody.email,
			image: userBody.image,
			dormId: userBody.dormId
		}
	});

	return Response.json(updatedUser);
};
