import { useMutation, useQueryClient } from '@tanstack/react-query';
import router from 'next/router';

import { type Product } from '@/types';

export const useCreateProductMutation = () => {
	const queryClient = useQueryClient();

	return useMutation({
		mutationKey: ['product'],
		mutationFn: async (product: Product) => {
			const response = await fetch(`/api/auth/products`, {
				method: 'POST',
				body: JSON.stringify(product)
			});
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			return response.json();
		},
		onMutate: async (newProduct: Product) => {
			await queryClient.cancelQueries({ queryKey: ['products'] });
			const previousProducts = queryClient.getQueryData<Product[]>([
				'products'
			]);
			if (previousProducts) {
				queryClient.setQueryData<Product[]>(
					['products'],
					(old: Product[] | undefined) => [...(old ?? []), newProduct]
				);
			}
			return { previousProducts };
		},
		onSuccess: async () => {
			await queryClient.invalidateQueries({ queryKey: ['products'] });
			router.push('/inventory');
		}
	});
};
