import { useState } from 'react';
import type * as Form from '@radix-ui/react-form';
import { type Control, Controller, type FieldValues } from 'react-hook-form';
import { Button, Card } from '@radix-ui/themes';
import { FaTrash } from 'react-icons/fa';

type ImageSelectorProps = {
	control: Control<FieldValues>;
} & React.ComponentProps<typeof Form.Root>;

const validFileTypes = [
	'image/svg+xml',
	'image/png',
	'image/jpg',
	'image/jpeg',
	'image/gif'
];
const allowedAspectRatio = 4 / 3;

const isValidFileType = (file: File): boolean =>
	validFileTypes.includes(file.type);

const isValidAspectRatio = async (file: File): Promise<boolean> =>
	new Promise(resolve => {
		const image: HTMLImageElement = new window.Image();
		image.onload = () => {
			const aspectRatio = image.width / image.height;
			resolve(Math.abs(aspectRatio - allowedAspectRatio) < 0.01);
		};
		image.src = URL.createObjectURL(file);
	});

const ImageSelector: React.FC<ImageSelectorProps> = ({ control }) => {
	const [selectedFile, setSelectedFile] = useState<File | null>(null);
	const [error, setError] = useState<string | null>(null);

	return (
		<div>
			<Controller
				control={control}
				name="photo"
				render={({ field: { onChange } }) => (
					<Card className="flex w-96 items-center justify-center ">
						{!selectedFile ? (
							<>
								<label
									htmlFor="dropzone-file"
									className="flex h-64 w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-gray-300 bg-gray-50 opacity-95 hover:bg-gray-100 dark:border-gray-600 dark:bg-zinc-900 dark:hover:border-gray-500 dark:hover:bg-gray-600"
								>
									<div className="flex flex-col items-center justify-center pb-6 pt-5">
										<svg
											className="mb-4 h-8 w-8 text-gray-500 dark:text-gray-400"
											aria-hidden="true"
											xmlns="http://www.w3.org/2000/svg"
											fill="none"
											viewBox="0 0 20 16"
										>
											<path
												stroke="currentColor"
												strokeLinecap="round"
												strokeLinejoin="round"
												strokeWidth="2"
												d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"
											/>
										</svg>
										<p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
											<span className="font-semibold">Click to upload</span> or
											drag and drop
										</p>
										<p className="text-xs text-gray-500 dark:text-gray-400">
											SVG, PNG, JPG, or GIF (MAX. 800x400px)
										</p>
									</div>
								</label>

								<input
									id="dropzone-file"
									type="file"
									onChange={async e => {
										const file = e.target.files?.[0] ?? null;
										if (file) {
											if (
												isValidFileType(file) &&
												(await isValidAspectRatio(file))
											) {
												onChange(file);
												setSelectedFile(file);
												setError(null);
											} else {
												setError(
													'Invalid file type or aspect ratio. Please select a valid image file with a 4:3 aspect ratio.'
												);
											}
										}
									}}
									className="hidden"
								/>
							</>
						) : (
							<div className="relative h-auto">
								<div
									style={{
										position: 'relative',
										overflow: 'hidden',
										paddingTop: '75%' // Smaller image preview size
									}}
								>
									<img
										src={URL.createObjectURL(selectedFile)}
										alt="placeholder"
										className="absolute inset-0 h-full w-full rounded-lg object-cover object-center"
									/>
								</div>
								<Button
									type="button"
									color="ruby"
									variant="solid"
									onClick={() => {
										setSelectedFile(null);
										onChange(null);
										setError(null);
									}}
									className="absolute right-2 top-2 z-10 "
								>
									<FaTrash className="m-2" />
								</Button>
							</div>
						)}
						{error && <p className="mt-2 text-ruby-red-400">{error}</p>}
					</Card>
				)}
			/>
		</div>
	);
};

export default ImageSelector;
