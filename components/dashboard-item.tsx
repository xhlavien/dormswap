import React from 'react';
import {
	Avatar,
	Box,
	Button,
	Card,
	Flex,
	Heading,
	Link,
	Text
} from '@radix-ui/themes';
import Image from 'next/image';
import { redirect } from 'next/navigation';

import { type Listing, type Product } from '@/types';
import { getServerAuthSession } from '@/server/auth';
import { db } from '@/server/db';

type ListingItemProps = {
	listing: Listing;
	product: Product;
};

const DashboardItem = async ({ listing, product }: ListingItemProps) => {
	const status = await getServerAuthSession();
	if (!status) {
		// User unauthenticated, redirect to login
		redirect('/login');
	}

	const user = await db.user.findUnique({ where: { id: listing.userId } });

	return (
		<Box>
			<Card className="max-w-[600px] rounded-md bg-white p-6 shadow-md">
				<Flex direction="column" gap="4">
					{user && (
						<Flex align="center" gap="2">
							<Avatar
								src={user.image ?? undefined}
								alt={user.name ?? undefined}
								size="3"
								fallback=""
							/>
							<Text>{user.name}</Text>
						</Flex>
					)}

					<Heading as="h1" size="4" className="mb-2 text-3xl font-bold">
						{product.name}
					</Heading>

					<Text size="3" className="mb-4 text-gray-500">
						{product.description}
					</Text>

					<Image
						className="h-auto max-h-[75%] w-full rounded-md object-cover shadow-md"
						src={product.photo}
						alt={product.name}
						width={200}
						height={200}
					/>
				</Flex>
			</Card>
			<Link
				style={{ color: 'inherit', cursor: 'pointer' }}
				key={listing.listingId}
				href={`/dashboard/detail/${listing.listingId}`}
			>
				<Button style={{ cursor: 'pointer' }} radius="none" className="w-full" disabled={user?.id === status.user.id}>
					Open listing
				</Button>
			</Link>
		</Box>
	);
};

export default DashboardItem;
