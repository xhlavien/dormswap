import { ImageResponse } from 'next/og';
import Image from 'next/image'; // Import the Image component

// Route segment config
export const runtime = 'edge';

// Image metadata
export const alt = 'Dormswap logo';
export const size = {
	width: 1200,
	height: 630
};

export const contentType = 'image/png';

// Image generation
export const OgImage = async () =>
	new ImageResponse(
		(
			<div
				style={{
					fontSize: 128,
					background: 'linear-gradient(#FFFFFF, grey)',
					width: '100%',
					height: '100%',
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					flexDirection: 'column'
				}}
			>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'center'
					}}
				>
					<svg
						viewBox="0 0 100.425 100.624"
						style={{ mixBlendMode: 'darken', filter: 'none' }}
						xmlns="http://www.w3.org/2000/svg"
					>
						<g
							style={{ filter: 'none', mixBlendMode: 'color-burn' }}
							transform="matrix(0.9999989867210389, 0, 0, 1, -0.0000010000001093857236, -0.000001000000110273902)"
						>
							<path
								d="M28.538,25.769h29.077c1.523,0,2.77-1.246,2.77-2.769v-8.308c0-1.523-1.246-2.769-2.77-2.769H28.538   c-1.523,0-2.769,1.246-2.769,2.769V23C25.769,24.523,27.015,25.769,28.538,25.769z"
								style={{
									fillRule: 'nonzero',
									fill: 'rgb(56, 178, 172)',
									shapeRendering: 'crispedges',
									paintOrder: 'fill'
								}}
							/>
							<path
								d="M92.23,5h-8.307c-1.523,0-2.77,1.246-2.77,2.769v24.923H18.846V7.769C18.846,6.246,17.6,5,16.077,5H7.769   C6.246,5,5,6.246,5,7.769V92.23C5,93.754,6.246,95,7.769,95h8.308c1.523,0,2.769-1.246,2.769-2.77v-4.153h62.308v4.153   c0,1.523,1.246,2.77,2.77,2.77h8.307c1.523,0,2.77-1.246,2.77-2.77V7.769C95,6.246,93.754,5,92.23,5z M81.153,74.23H18.846V46.539   h62.307h0.001L81.153,74.23L81.153,74.23z"
								style={{
									fillRule: 'nonzero',
									fill: 'rgb(56, 178, 172)',
									shapeRendering: 'crispedges',
									paintOrder: 'fill'
								}}
							/>
							<path
								d="M57.615,53.462H28.538c-1.523,0-2.769,1.246-2.769,2.77v8.307c0,1.523,1.246,2.77,2.769,2.77h29.077   c1.523,0,2.77-1.246,2.77-2.77v-8.307C60.385,54.708,59.139,53.462,57.615,53.462z"
								style={{
									fillRule: 'nonzero',
									fill: 'rgb(56, 178, 172)',
									shapeRendering: 'crispedges',
									paintOrder: 'fill'
								}}
							/>
						</g>
					</svg>
					Dormswap
				</div>

				<span style={{ fontSize: '2rem', textTransform: 'uppercase' }}>
					Trade your stuff with other Brno students
				</span>
			</div>
		),
		{
			...size
		}
	);
export default OgImage;
