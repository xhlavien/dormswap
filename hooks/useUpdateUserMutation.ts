import { useMutation } from '@tanstack/react-query';
import { type User } from '.prisma/client';

export const useUpdateUserMutation = () =>
	useMutation({
		mutationFn: (user: User) =>
			fetch(`api/auth/user/${user.id}`, {
				method: 'PATCH',
				body: JSON.stringify(user)
			})
				.then(res => res.json())
				.then(res => res as User)
	});
