import { useMutation, useQueryClient } from '@tanstack/react-query';
import router from 'next/router';

import { type Product } from '@/types';

export const useDeleteProductMutation = () => {
	const queryClient = useQueryClient();
	return useMutation({
		mutationKey: ['products'],
		mutationFn: async (productId: number) => {
			await fetch(`api/auth/products/${productId}`, {
				method: 'DELETE',
				body: JSON.stringify(productId)
			})
				.then(res => res.json())
				.then(res => res as Product);
		},
		onMutate: async (productId: number) => {
			const previousProducts = queryClient.getQueryData<Product[]>([
				'products'
			]);
			queryClient.setQueryData<Product[]>(['products'], old =>
				old?.filter((product: Product) => product.productId !== productId)
			);
			return { previousProducts };
		},
		onError: (err, _productId, context) => {
			console.error(err);
			queryClient.setQueryData<Product[]>(
				['products'],
				context?.previousProducts
			);
		},
		onSettled: async () => {
			await queryClient.invalidateQueries({ queryKey: ['products'] });
			router.push('/inventory');
		}
	});
};
