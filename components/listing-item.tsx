import React from 'react';
import {
	Avatar,
	Box,
	Button,
	Card,
	Flex,
	Heading,
	Link,
	Text
} from '@radix-ui/themes';
import Image from 'next/image';
import { type Listing, type Product } from '.prisma/client';
import router from 'next/router';
import { useSession } from 'next-auth/react';

import { useDeleteListingMutation } from '@/hooks/useDeleteListingMutation';

type ListingItemProps = {
	listing: Listing;
	product: Product;
};

const ListingItem = ({ listing, product }: ListingItemProps) => {
	const { data, status } = useSession();
	const { mutate: deleteListing } = useDeleteListingMutation();

	return (
		<Box>
			<Card className="max-w-[600px] bg-white p-6 shadow-md">
				<Flex direction="column" gap="4">
					{data?.user && (
						<Flex align="center" gap="2">
							<Avatar
								src={data?.user.image ?? undefined}
								alt={data?.user.name ?? undefined}
								size="3"
								fallback=""
							/>
							<Text className="text-gray-500">{data?.user.name}</Text>
						</Flex>
					)}

					<Heading as="h1" size="4" className="mb-2 text-3xl font-bold text-black">
						{product.name}
					</Heading>

					<Text size="3" className="mb-4 text-gray-500">
						{product.description}
					</Text>

					<Image
						className="h-auto max-h-[75%] w-full rounded-md object-cover shadow-md"
						src={product.photo}
						alt={product.name}
						width={200}
						height={200}
					/>
				</Flex>
			</Card>
			<Link
				style={{ color: 'inherit', cursor: 'pointer' }}
				key={listing.listingId}
				href={`/dashboard/detail/${listing.listingId}`}
			>
				<Button style={{ cursor: 'pointer' }} radius="none" className="w-full">
					Open listing
				</Button>
			</Link>
			{listing.userId === data?.user.id && (
				<Button
					style={{ cursor: 'pointer' }}
					onClick={() => deleteListing(listing.listingId)}
					radius="none"
					className="w-full"
					color="ruby"
				>
					Delete
				</Button>
			)}
		</Box>
	);
};

export default ListingItem;
