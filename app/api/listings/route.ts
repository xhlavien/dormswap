import { getServerSession } from 'next-auth';
import { type NextRequest, NextResponse } from 'next/server';

import { authOptions } from '@/server/auth';
import { db } from '@/server/db';

export const GET = async (req: NextRequest) => {
	const session = await getServerSession(authOptions);

	if (!session?.user?.id)
		return NextResponse.json(
			{ status: 401, error: 'NOT_AUTHORIZED' },
			{ status: 401 }
		);

	try {
		const result = await db.listing.findMany({
			include: {
				productForExchange: true
			}
		});
		return NextResponse.json({ status: 200, result }, { status: 200 });
	} catch (e) {
		return NextResponse.json(
			{ status: 400, error: 'LISTING_NOT_FOUND' },
			{ status: 400 }
		);
	}
};
