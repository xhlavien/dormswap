import { Heading } from '@radix-ui/themes';
import React from 'react';
import { type Listing } from '.prisma/client';

import { db } from '@/server/db';
import { getServerAuthSession } from '@/server/auth';
import { type Product } from '@/types';

import DashboardItemContainer from './dashboard-item-container';
type ListingListProps = {
	listings: Listing[];
	query: string | undefined;
};
const filter = async (
	listing: Listing,
	product: Product | null,
	query: string | undefined
) => {
	if (!query) {
		return listing;
	}

	const user = await db.user.findUnique({ where: { id: listing.userId } });
	console.log(user?.name);
	if (!query) return true;
	if (
		listing.description.includes(query) ||
		user?.name?.includes(query) ||
		product?.name.includes(query) ||
		product?.description.includes(query)
	)
		return true;
	else return false;
};

const DashboardList = async ({ listings, query }: ListingListProps) => (
	<div>
		{listings.length < 1 ? (
			<Heading>No listings available...</Heading>
		) : (
			<div className="flex flex-wrap justify-center gap-12">
				{await Promise.all(
					listings.map(async l => {
						const product = await db.product.findUnique({
							where: { productId: l.offeredProductId }
						});
						if (product && (await filter(l, product, query)))
							return (
								<DashboardItemContainer
									key={l.listingId}
									listing={l}
									product={product}
								/>
							);
					})
				)}
			</div>
		)}
	</div>
);

export default DashboardList;
