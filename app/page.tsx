'use client';
import {
	Flex,
	Text,
	Button,
	Card,
	Switch,
	Container,
	Box,
	Heading
} from '@radix-ui/themes';
import { List } from '@radix-ui/react-tabs';

const Home = () => (
	<Container size="1" height="max-content">
		<Box m="auto">
			<Card>
				<Flex direction="column" gap="2">
					<Heading className="justify-center">Welcome to DormSwap!</Heading>
					<Text>
						Brand new application for exchanging items between students.
					</Text>
					<Text>How to use this app:</Text>
					<Text>1. Sign In using your Discord account</Text>
					<Text>
						2. Finish setting up your profile (first name, last name, email,
						phone number required)
					</Text>
					<Text>
						3. Add some products to your inventory that you want to exchange
					</Text>
					<Text>
						4. Check out the listings of other people on the Dashboard
					</Text>
					<Text>5. React to other user&apos;s listings or make your own!</Text>
				</Flex>
			</Card>
		</Box>
	</Container>
);
export default Home;
