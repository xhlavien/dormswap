import { type NextRequest, NextResponse } from 'next/server';
import { getServerSession } from 'next-auth';

import { db } from '@/server/db';
import { authOptions } from '@/server/auth';

export const GET = async (
	_: NextRequest,
	{ params: { listingId } }: { params: { listingId: string } }
) => {
	const id = parseFloat(listingId);
	const listing = await db.listing.findUnique({
		where: {
			listingId: id
		},
		include: {
			offeredProduct: true,
			productForExchange: true,
			user: true
		}
	});

	const owner = await db.user.findUnique({
		where: {
			id: listing?.user?.id
		}
	});
	const trader = await db.user.findUnique({
		where: {
			id: listing?.productForExchange?.ownerId
		}
	});
	if (!listing || !owner || !trader) {
		return NextResponse.json({
			status: 404,
			error: 'NOT_FOUND'
		});
	}

	return NextResponse.json({
		status: 200,
		result: {
			listing,
			owner,
			trader
		}
	});
};
