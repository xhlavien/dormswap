'use client';
import React from 'react';
import { type Listing } from '.prisma/client';

import { useGetProductByIdQuery } from '@/hooks/useGetProductByIdQuery';
import LoadingSpinner from '@/components/loading-spinner';
import ListingItem from '@/components/listing-item';

type ListingItemContainerProps = {
	listing: Listing;
};

const ListingItemContainer = ({ listing }: ListingItemContainerProps) => {
	const {
		data: product,
		isLoading: isLoadingProduct,
		isError: isErrorProduct
	} = useGetProductByIdQuery(listing.offeredProductId);

	if (isLoadingProduct) {
		return <LoadingSpinner label="Loading..." />;
	}

	if (!product || isErrorProduct) {
		return <h2 className="text-white">Error loading product.</h2>;
	}

	return <ListingItem listing={listing} product={product} />;
};

export default ListingItemContainer;
