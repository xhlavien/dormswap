'use client';
import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { type Listing } from '.prisma/client';
import * as Dialog from '@radix-ui/react-dialog';
import { useQueryClient } from '@tanstack/react-query';
import { useRouter } from 'next/navigation';
import {
	Button,
	Card,
	Flex,
	TextArea,
	Box,
	SelectItem
} from '@radix-ui/themes';
import * as Select from '@radix-ui/react-select';
import { ChevronDownIcon } from '@radix-ui/react-icons';

import LoadingSpinner from '@/components/loading-spinner';
import { ListingSchema } from '@/schemas/listing-schema';
import { useCreateListingMutation } from '@/hooks/useCreateListingMutation';
import { useGetProducts } from '@/hooks/useGetProducts';

const ListingDialog = () => {
	const {
		register,
		formState: { errors },
		handleSubmit,
		control,
		setValue,
		getValues,
		clearErrors,
		reset
	} = useForm<Listing>({ resolver: zodResolver(ListingSchema) });

	const { mutate: createListing, isPending: isPendingCreateListing } =
		useCreateListingMutation();
	const [open, setOpen] = React.useState(false);

	const queryClient = useQueryClient();

	const router = useRouter();

	const {
		data: products,
		isLoading: isLoadingProducts,
		isError: isErrorProducts
	} = useGetProducts();

	const onSubmit = (listing: Listing) => {
		createListing(
			{ ...listing },
			{
				onSuccess: () => {
					console.log('success!');
					queryClient.removeQueries({ queryKey: ['listings'] });
					router.push('/listings');
				}
			}
		);
		setOpen(false);
		reset();
	};

	if (isLoadingProducts) {
		return <LoadingSpinner label="Loading products..." />;
	}

	if (isPendingCreateListing) {
		return <LoadingSpinner label="Updating information..." />;
	}

	if (!products || isErrorProducts) {
		return <h2>Error fetching products.</h2>;
	}

	const productsToOffer = products.filter(p => p.offeredInListing === null);

	return (
		<Dialog.Root open={open} onOpenChange={setOpen}>
			<Dialog.Trigger asChild>
				<Button>Create listing</Button>
			</Dialog.Trigger>
			<Dialog.Portal>
				<Dialog.Overlay className="fixed inset-0 bg-opacity-5 backdrop-blur-lg dark:bg-black" />
				<Dialog.Content className="fixed left-[50%] top-[50%] w-min min-w-[300px] translate-x-[-50%] translate-y-[-50%] bg-white text-black shadow-sm dark:bg-black dark:text-white">
					<form onSubmit={handleSubmit(onSubmit)}>
						<Card className="p-5">
							<Flex className="flex-col " justify="center">
								<Dialog.Title className="text-center text-lg">
									Create new listing
								</Dialog.Title>
								<Flex className=" flex-col gap-2 p-2">
									<Controller
										defaultValue={undefined}
										control={control}
										{...register('offeredProductId')}
										render={({ field }) => (
											<Select.Root
												onValueChange={value => {
													setValue('offeredProductId', Number(value));
													clearErrors('offeredProductId');
												}}
												{...field}
												value={
													getValues('offeredProductId')?.toString() || undefined
												}
												disabled={productsToOffer.length < 1}
											>
												<Select.Trigger
													className="inline-flex w-full cursor-pointer items-center justify-between gap-2 rounded bg-teal-500 p-2 shadow-md hover:bg-teal-500 focus:outline-none focus:ring-2 focus:ring-black"
													aria-label="Food"
												>
													<Select.Value placeholder="Select item..." />
													<Select.Icon>
														<ChevronDownIcon />
													</Select.Icon>
												</Select.Trigger>
												<Select.Portal>
													<Select.Content className="overflow-hidden rounded border bg-teal-500 shadow-lg dark:text-white">
														<Select.Viewport
															className="relative max-h-48 overflow-y-auto p-2"
															style={{ maxHeight: '5rem' }} // Set a fixed max height
														>
															{productsToOffer.map(p => (
																<SelectItem
																	value={p.productId.toString()}
																	key={p.productId}
																>
																	{p.name}
																</SelectItem>
															))}
														</Select.Viewport>
													</Select.Content>
												</Select.Portal>
											</Select.Root>
										)}
									/>
									<TextArea
										{...register('description', { required: true })}
										placeholder="Item Description"
										rows={3}
										className="bg-transparent p-2"
									/>
									{errors.description && (
										<p className="text-ruby-red-400">Invalid Description</p>
									)}
								</Flex>
							</Flex>
						</Card>
						<Box className="grid w-full grid-cols-2 ">
							<Button
								className="w-full "
								variant="solid"
								radius="medium"
								color="teal"
								type="submit"
							>
								Save
							</Button>
							<Dialog.Close asChild>
								<Button
									className="w-full"
									variant="solid"
									radius="medium"
									color="ruby"
									type="button"
								>
									Close
								</Button>
							</Dialog.Close>
						</Box>
					</form>
				</Dialog.Content>
			</Dialog.Portal>
		</Dialog.Root>
	);
};

export default ListingDialog;
