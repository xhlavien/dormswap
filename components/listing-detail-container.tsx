import React from 'react';
import { Button } from '@radix-ui/themes';

import LoadingSpinner from '@/components/loading-spinner';
import ListingDetail from '@/components/listing-detail';
import { db } from '@/server/db';
import OfferItem from '@/components/offer-item/offer-item';
import TradeDetail from '@/components/trade-detail';

type ListingDetailContainerProps = {
	userId: string;
	listingId: string;
};

const ListingDetailContainer = async ({
	userId,
	listingId
}: ListingDetailContainerProps) => {
	const listing = await db.listing.findUnique({
		where: { listingId: parseInt(listingId) }
	});
	// const router = useRouter();
	// const refreshPage = () => router.refresh();

	// const goBack = () => {
	// 	router.back(); // Add this function to navigate back to the previous page
	// };

	if (!listing) {
		return <h2>Listing error</h2>;
	}

	if (listing.productForExchangeId !== null) {
		return (
			<TradeDetail
				listing={listing}
				productForExchangeId={listing.productForExchangeId}
				userId={userId}
			/>
		);
	}

	return (
		<div>
			{/* <Button onClick={goBack}>&#8592; Back</Button> */}
			<ListingDetail listing={listing}>
				{listing.userId !== userId && <OfferItem listing={listing} />}
			</ListingDetail>
		</div>
	);
};

export default ListingDetailContainer;
