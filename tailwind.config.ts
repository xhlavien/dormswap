import type { Config } from 'tailwindcss';

const config: Config = {
	darkMode: 'class',
	content: [
		'./pages/**/*.{js,ts,jsx,tsx,mdx}',
		'./components/**/*.{js,ts,jsx,tsx,mdx}',
		'./app/**/*.{js,ts,jsx,tsx,mdx}'
	],
	theme: {
		extend: {
			colors: {
				'ruby-red': {
					'50': '#fef2f3',
					'100': '#fde6e9',
					'200': '#fad1d7',
					'300': '#f6abb6',
					'400': '#f07c90',
					'500': '#e54666',
					'600': '#d22c56',
					'700': '#b11f47',
					'800': '#941d42',
					'900': '#7f1c3d',
					'950': '#470a1d'
				},

				'persian-green': {
					'50': '#f0fdfa',
					'100': '#ccfbf1',
					'200': '#99f6e3',
					'300': '#5eead3',
					'400': '#2dd4be',
					'500': '#12a594',
					'600': '#0d9487',
					'700': '#0f766d',
					'800': '#115e59',
					'900': '#134e4a',
					'950': '#042f2e'
				},
				'midnight': {
					'50': '#eef9ff',
					'100': '#d9f0ff',
					'200': '#bce5ff',
					'300': '#8ed7ff',
					'400': '#59beff',
					'500': '#339ffe',
					'600': '#1c82f4',
					'700': '#1569e0',
					'800': '#1855b5',
					'900': '#194a8f',
					'950': '#0c1b33'
				}
			},
			backgroundImage: {
				'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
				'gradient-conic':
					'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))'
			}
		}
	},
	plugins: []
};
export default config;
